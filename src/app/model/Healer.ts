import {Perso} from "./Personnage";

export class Healer extends Perso {
  constructor(name: string) {
    super(name);
    this._pvMax = 150;
    this._pv = 150;
    this._magicPower = 80;
    this._attackPower = 10;
  }
}
