import {Perso} from "./Personnage";

export class Mage extends Perso {
  constructor(name: string) {
    super(name);
    this._magicPower = 70;
    this._attackPower = 25;
  }
}
