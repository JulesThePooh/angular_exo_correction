import {Perso} from "./Personnage";

export class Warrior extends Perso {

  constructor(name: string) {
    super(name);
    this._pvMax = 300;
    this._pv = 300;
    this._magicPower = 20;
    this._attackPower = 70;
  }


}
