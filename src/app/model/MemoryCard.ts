export class MemoryCard {
  private _id: number;
  private _image: string;
  private _hasBeenFound: boolean = false;
  private _isReturn: boolean = false;


  constructor(id: number, image: string) {
    this._id = id;
    this._image = image;
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get image(): string {
    return this._image;
  }

  set image(value: string) {
    this._image = value;
  }

  get hasBeenFound(): boolean {
    return this._hasBeenFound;
  }

  set hasBeenFound(value: boolean) {
    this._hasBeenFound = value;
  }

  get isReturn(): boolean {
    return this._isReturn;
  }

  set isReturn(value: boolean) {
    this._isReturn = value;
  }
}
