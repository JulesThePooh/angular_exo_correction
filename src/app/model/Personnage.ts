export class Perso {
  private _name: string;
  protected _pvMax: number = 200;
  protected _pv: number = 200;
  protected _magicPower: number = 45;
  protected _attackPower: number = 45;


  constructor(name: string) {
    this._name = name;
  }

  public attack(perso: Perso) {
    // let atq = this.
    //toto attack here
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get pvMax(): number {
    return this._pvMax;
  }

  set pvMax(value: number) {
    this._pvMax = value;
  }

  get pv(): number {
    return this._pv;
  }

  set pv(value: number) {
    this._pv = value;
  }

  get magicPower(): number {
    return this._magicPower;
  }

  set magicPower(value: number) {
    this._magicPower = value;
  }

  get attackPower(): number {
    return this._attackPower;
  }

  set attackPower(value: number) {
    this._attackPower = value;
  }
}
