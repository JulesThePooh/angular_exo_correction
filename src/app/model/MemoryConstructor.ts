import {MemoryCard} from "./MemoryCard";

export class MemoryConstructor {

  private imageArray: Array<string> = [
    'https://i.picsum.photos/id/78/200/200.jpg?hmac=P2qNtvmWActric-MfeYNdsN7YuChCioX-9CkJMNUYpk',
    'https://i.picsum.photos/id/796/200/200.jpg?hmac=TabKFVb5_IyNIu3LHpgEW6YnI0AxHo3G6fyHubk1OY8',
    'https://i.picsum.photos/id/1052/200/200.jpg?hmac=C8TAQ7jOmsdTxY6LFqx0ft2jNVIX0GxUmo8kCnVHkIE',
    'https://i.picsum.photos/id/227/200/200.jpg?hmac=_HAD3ZQuIUMd1tjQfU5i21RCLHRDH_r_Xuq0q6iRN-o',
    'https://i.picsum.photos/id/472/200/200.jpg?hmac=PScxKeNxgxcauarhbWIWesyo4VsouCtfdX8fNTy9HRI',
    'https://i.picsum.photos/id/910/200/200.jpg?hmac=5y7FBcwrEQLaT1hO3VufjbQNxx_eg-znDlA1JclTpDQ',
    'https://i.picsum.photos/id/686/200/200.jpg?hmac=5DMCllhAJj0gbXXcSZQLQZwnruDJDMVbmFqqwZ6wFug',
    'https://i.picsum.photos/id/643/200/200.jpg?hmac=ouS38xYuy8iE3e24i3dNN11vJoBa6kKr3HzduEJ5Msk',
    'https://i.picsum.photos/id/84/200/200.jpg?hmac=6H-uafgNQmg74KSd7tSKVP1PWLigkAnXdB_PyFgxXNA',
    'https://i.picsum.photos/id/11/200/200.jpg?hmac=LBGO0uEpEmAVS8NeUXMqxcIdHGIcu0JiOb5DJr4mtUI',
    'https://i.picsum.photos/id/244/200/200.jpg?hmac=Q1gdvE6ZPZUX3nXkxvmzuc12eKVZ9XVEmSH3nCJ2OOo',
    'https://i.picsum.photos/id/1035/200/200.jpg?hmac=IDuYUZQ_7a6h4pQU2k7p2nxT-MjMt4uy-p3ze94KtA4'
  ]

  generateArrayOfCards(numberCard: number): Array<MemoryCard> {

    let array: Array<MemoryCard> = [];

    for (let i: number = 0; i < numberCard; i++) {
      let card = new MemoryCard(i, this.imageArray[i]);
      let card2 = new MemoryCard(i, this.imageArray[i]);

      array.push(card);
      array.push(card2);
    }

    return array
      .map(value => ({value, sort: Math.random()}))
      .sort((a, b) => a.sort - b.sort)
      .map(({value}) => value);
  }
}
