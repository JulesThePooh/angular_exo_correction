export class Student {

  private _name: string;
  private _age: number;
  private _sexe: string;
  private _activities: Array<string>;


  constructor(name: string, age: number, sexe: string, activities: Array<string>) {
    this._name = name;
    this._age = age;
    this._sexe = sexe;
    this._activities = activities;
  }


  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get age(): number {
    return this._age;
  }

  set age(value: number) {
    this._age = value;
  }

  get sexe(): string {
    return this._sexe;
  }

  set sexe(value: string) {
    this._sexe = value;
  }

  get activities(): Array<string> {
    return this._activities;
  }

  public getStringActivities(): string {
    let string: string = '';

    let loopIndex: number = 1;
    this.activities.forEach((activity: string) => {

      string += activity;

      if (loopIndex < this.activities.length) {
        string += ', ';
      }

      loopIndex++;
    })

    return string;
  }
}
