import {Component, OnInit} from '@angular/core';
import {Student} from "../../model/Student";

@Component({
  selector: 'app-exo3',
  templateUrl: './exo3.component.html',
  styleUrls: ['./exo3.component.scss']
})
export class Exo3Component implements OnInit {

  public _students: Array<Student> = [];

  constructor() {
  }

  ngOnInit(): void {
    let jules: Student = new Student('Jules', 31, 'homme', ['t', 'r', 'f']);
    let maxime: Student = new Student('Maxime', 30, 'homme', ['n', 'j', 'v']);
    let kevin: Student = new Student('Kevin', 32, 'homme', ['b', 'c', 'e']);
    let alex: Student = new Student('Alex', 20, 'homme', ['a', 'n', 'x']);

    this._students = [jules, maxime, kevin, alex];
  }

}
