import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-exo1',
  templateUrl: './exo1.component.html',
  styleUrls: ['./exo1.component.scss']
})
export class Exo1Component implements OnInit {

  public _counter: number = 0;

  constructor() {
  }

  ngOnInit(): void {

  }

  public addNumberToCounter(number: number): void {
    this._counter += number;
  }

}
