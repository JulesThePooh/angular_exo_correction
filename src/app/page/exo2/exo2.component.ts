import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-exo2',
  templateUrl: './exo2.component.html',
  styleUrls: ['./exo2.component.scss']
})
export class Exo2Component implements OnInit {

  public _games: Array<string> = ['Elden Ring', 'Zelda', 'Mario', 'Sonic'];
  public _basket: Array<string> = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  public addToBasket(game: string): void {
    this._basket.push(game);
  }

  public removeGameFromBasket(game: string): void {
    let position: number = this._basket.indexOf(game);
    this._basket.splice(position, 1);
  }

}
