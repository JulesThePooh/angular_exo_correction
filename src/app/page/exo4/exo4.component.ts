import {Component, OnInit} from '@angular/core';
import {MemoryConstructor} from "../../model/MemoryConstructor";
import {MemoryCard} from "../../model/MemoryCard";

@Component({
  selector: 'app-exo4',
  templateUrl: './exo4.component.html',
  styleUrls: ['./exo4.component.scss']
})
export class Exo4Component implements OnInit {

  public _memoryCards: Array<MemoryCard> = [];
  private _memoryConstructor = new MemoryConstructor();
  private _cardReturned: Array<MemoryCard> = [];

  constructor() {
  }

  ngOnInit(): void {
    this.restartGame(6);

  }

  restartGame(numberCard: number): void {
    this._cardReturned = [];
    this._memoryCards = this._memoryConstructor.generateArrayOfCards(numberCard);
  }

  returnCard(card: MemoryCard) {
    if (!card.hasBeenFound) {

      card.isReturn = true
      this._cardReturned.push(card);

      if (this._cardReturned.length === 2) {
        if (this._cardReturned[0].id === this._cardReturned[1].id) {
          this._cardReturned[0].hasBeenFound = true;
          this._cardReturned[1].hasBeenFound = true;
        }
      }

      if (this._cardReturned.length > 2) {

        this._cardReturned.forEach((innerCard: MemoryCard) => {
          innerCard.isReturn = false;
        })
        this._cardReturned = [];

      }
    }
  }

}
